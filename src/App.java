import model.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem();
        invoiceItem1.setId("12bcd");
        invoiceItem1.setQty(10);
        invoiceItem1.setDesc("May bom");
        invoiceItem1.setUnitPrice(120000);
        InvoiceItem invoiceItem2 = new InvoiceItem("23BDC", "May Lanh", 12, 120000);
        System.out.println("Tong gia item1: " + invoiceItem1.getTotal());
        System.out.println("Tong gia item2: " + invoiceItem2.getTotal());
        System.out.println("item1:" + invoiceItem1.toString());
        System.out.println("item2:" + invoiceItem2.toString());

    }
}
